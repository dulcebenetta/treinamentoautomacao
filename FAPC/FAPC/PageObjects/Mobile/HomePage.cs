﻿using OpenQA.Selenium;

namespace FAPC.PageObjects.Mobile
{
    public class HomePage : TestBase
    {
        public static By ClickPesquisar(int timeout = DefaultTimedOut)
        {
            return By.XPath("//android.widget.ImageView[@content-desc='Pesquisar']").Click();
        }
    }
}